<?php
/**
 * Powerform class
 * @author mfostrander
 * @date 2018-01-23
 * Initiates a Docusign Powerform
 */


class Powerform {

	/**
	 * private $formRecipients is the array of recipients to be set in the initiation of the Powerform
	 */
	private $formRecipients = array();
	/**
	 * private $formData contains the data keys and values to be automatically injected into the form.
	 */
	private $formData = array();
	/**
	 * private $PowerformID is set upon initiating the Powerform class to the ID of the output form
	 */
	private $PowerformID; 
	private $PowerformURL = 'https://www.docusign.net/Member/PowerFormSigning.aspx';
	public function __construct($id) {
		if(!$id) throw new Exception('Invalid Powerform ID');
		$this->PowerformID = $id;
	}

	public function addRecipient($role, $name, $email) {
		if((!$role) || (!$name) || (!$email)) throw new Exception('Invalid: you must specify a role, name, and email.');
		$this->formRecipients[$role.'_UserName'] = $name;
		$this->formRecipients[$role.'_Email'] = $email;
		return true;
	}

	public function addField($formElement, $value) {
		if((!$formElement) || (!$value)) throw new Exception('Invalid: you must specify a formElement and value.');
		$this->formData['EnvelopeField_'.$formElement] = $value;
		return true;
	}
	private function prepareForm() {
		$data = array_merge($this->formRecipients, $this->formData);
		$params = http_build_query($data);
		return '?PowerFormId='.$this->PowerformID.'&'.$params;
	}

	public function display() {
		$data = $this->prepareForm();
		header('Location: '.$this->PowerformURL.$data);
	}
	public function getURL() {
		$data = $this->prepareForm();
		return $this->PowerformURL.$data;
	}

}
?>