<!doctype html>
<html lang="en">
  <head>
    <title>SON Major Bypass</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <link rel="stylesheet" href="https://digitalsignage.uaa.alaska.edu/assets/lib/css/formValidation.min.css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <script src="https://rubaxa.github.io/Sortable/Sortable.js"></script>

<script
  src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
     <script src="//code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>

    <style>
.top {
	margin-top: 50px;
}
    </style>
  </head>
  <body>
	<div class="container">
		<div class="row top">
			<div class="col-sm">
				<form class="form-horizontal">
					<fieldset>
						<div class="form-group row">
							<label class="col-2 control-label" for="email">Form ID</label>  
							<div class="col-10">
								<input id="formID" name="formID" type="text" placeholder="345sdfqasd0-234asf2-gh212a-sd234" class="form-control input-md" required="">	    
							</div>
						</div>
						<div class="form-group row">
							<label class="col-2 control-label" for="email">UA Email Address</label>  
							<div class="col-10">
								<input id="email" name="email" type="text" placeholder="student@alaska.edu" class="form-control input-md" required="">	    
							</div>
						</div>
						<div class="form-group row">
							<label class="col-2 control-label" for="studentName">Student Name</label>  
							<div class="col-10">
							  	<input id="studentName" name="studentName" type="text" placeholder="Sea Wolf" class="form-control input-md" required="">
						  	</div>
						</div>
					</fieldset>
				</form>

				<button class="btn btn-primary" id="generateURL" onclick="generateURL();">Generate Link for Student</button>
				<br><br><br>
				<div id="result">
				<code>
				</code>
				</div>
			</div>
		</div>
	</div>

	<script>
	function generateURL() {
		var URL = 'major.php?auth=complete&bypass=true&id='+$('#formID').val()+'&name='+$('#studentName').val()+'&email='+$('#email').val();
		$('#result > code').text(URL);
	}
	</script>
	<script src="https://digitalsignage.uaa.alaska.edu/assets/lib/js/formValidation.min.js"></script>
	<script src="https://digitalsignage.uaa.alaska.edu/assets/lib/js/framework/bootstrap4.min.js"></script>
	<script src="https://digitalsignage.uaa.alaska.edu/assets/lib/js/framework/bootstrap.min.js"></script>
	
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
        <script src="//cdn.jsdelivr.net/npm/jquery.scrollto@2.1.2/jquery.scrollTo.min.js"></script>

  </body>
</html>
