<?php
/**
 * Init change of major form -- randomly select advisor
 * @author mfostrander
 * @date: 2018-1-23
 */
session_start();
require_once('/var/simplesamlphp/lib/_autoload.php');
require_once('powerform.php');
require_once('advisor.php');
$ap =  new SimpleSAML_Auth_Simple('UAA-LDAP');
$urlpath = 'https://something.alaska.edu/major/';

$id = filter_var(@$_GET['id']);
$authStatus = filter_var(@$_GET['auth']);
$bypass = filter_var(@$_GET['bypass']);
$studentName = filter_var(@$_GET['name']);
$studentEmail = filter_var(@$_GET['email']);
$displayOnly = filter_var(@$_GET['displayOnly']);
if(!$id) die('Invalid form ID. Try again.');


if(($id) && ($authStatus != "complete")) {
	$ap->requireAuth();
	$attributes = $ap->getAttributes();
	$SSOArray['uakEmployeeCampus'] = $attributes['uaPrimaryAffil'][0];
	$SSOArray['eduPersonUniqueID'] = $attributes['employeeNumber'][0];
	$SSOArray['eduPersonPrincipalName'] = $attributes['userPrincipalName'][0];
	$SSOArray['mail'] = $attributes['mail'][0];
	$SSOArray['uaUsername'] = $attributes['sAMAccountName'][0];
	$SSOArray['displayName'] = $attributes['displayName'][0];
	$SSOArray['uakEmployeeAffiliation'] = 'Unavailable';
	$SSOArray['department'] = $attributes['department'][0];
	$SSOArray['tel'] = $attributes['telephoneNumber'][0];
	$session = SimpleSAML_Session::getSessionFromRequest();
	$session->cleanup();
	$_SESSION['SSO'] = $SSOArray;
	header('Location: '.$urlpath.'major.php?auth=complete'.'&id='.$id.'&displayOnly='.$displayOnly);
} 
if(($id) && ($authStatus == "complete")) {
	/**
	 * Create a new instance of Advisor()
	 * Sets up who will become the student's advisor
	 */
	$Advisor = new Advisor();
	//AAS - https://www.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=aa643c15-414c-4e26-aad3-d92a14cbfcf6
	//BS - https://www.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=c3d1eb87-c76f-4ce3-85c8-f37eeac6b25c
	// Select an advisor at random
	$routeTo = $Advisor->select();

	try {
		$p = new Powerform($id);
		$p->addRecipient('Advisor', $routeTo['name'], $routeTo['email']);
		if($bypass == "true") $p->addRecipient('Student', $studentName, $studentEmail);
		else $p->addRecipient('Student', $_SESSION['SSO']['displayName'], $_SESSION['SSO']['uaUsername'].'@alaska.edu');
		if($displayOnly == 'true') echo '<a href="'.$p->getURL().'">'.$p->getURL().'</a>';
		else echo $p->display();
	} catch (Exception $e) {
		echo $e->getMessage();
	}
}

if($authStatus == "logout") {
	session_unset();
	session_destroy();
	$_SESSION = array();
	$ap->logout($urlpath.'major.php?'.'id='.$id.'&displayOnly='.$displayOnly);
}

?>
