<?php
/**
 * Advisor: -- randomly select advisor from array
 * @author mfostrander
 * @date: 2018-1-23
 */
class Advisor {
	public $advisorArray;

	public function __construct() {
		$advisor[0] = array('name' => 'Advisor 1', 'email' => 'ad1@alaska.edu');
		$advisor[1] = array('name' => 'Advisor 2', 'email' => 'ad2@alaska.edu');
		$advisor[2] = array('name' => 'Advisor 3', 'email' => 'ad3@alaska.edu');
		$this->advisorArray = $advisor;
	}

	public function select() {
		$advisorID = array_rand($this->advisorArray);
		return $this->advisorArray[$advisorID];
	}
}

?>
